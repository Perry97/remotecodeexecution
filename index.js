const express = require('express');
const process = require('process')
const { exec } = require('child_process')
const fs = require('fs');
const path = require('path');
const { stdout, stderr } = require('process');
const cors = require('cors');
app = express();
app.use(cors())
require('dotenv').config()


async function dockerRunCode(id, command){
    return new Promise(((resolve,reject) =>{
        exec(command, {timeout:10000,killSignal:'SIGKILL'},(err, stdout,stderr) =>{
            if(stderr){
                reject(stderr);
            }
            if(err){
                if(err.killed){
                    exec(`docker kill ${id} `)
                    resolve(`${stdout} timeout `)
                }else{
                    exec(`docker kill ${id} `)
                    reject('Malicious code detected')
                }
            }else{
                resolve(stdout);
            }


        })

    }))

}
app.get('/', (req, res) => {
    res.send('Hello')
});
app.get('/python/', (req, res) => {
    const code = req.query.code;
    console.log("Code from req:" + code)
    fs.writeFile('./pythonRCE/code/code.py', code, function (err) {
        if (err) throw console.log(err);
        console.log('Success');
    });
    let id = (Math.random()+1).toString(36).substring(7) 
    let command = `docker run --rm -m 32m --memory-swap 64m --name  ${id} -v ${__dirname}/pythonRCE/code:/code -w /code pythonrce`
    let p = dockerRunCode(id,command);
    p.then(message =>{
        fs.unlink('./pythonRCE/code/code.py', (err) => {
            if (err) { console.log('Error in unlink: ', err.message) }
        });
        console.log(message);
        res.json({message: message});
    }).catch((err) =>{
        fs.unlink('./pythonRCE/code/code.py', (err) => {
            if (err) { console.log('Error in unlink: ', err.message) }
        });
        console.log('ERROR:', err); 
        res.json({message: err});
    })
    console.log('Container id: ', id)

    console.log(__dirname)

})
app.get('/java', (req, res) => {
    res.send('Java response')
})

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Listening on port: ${PORT}`);
})
